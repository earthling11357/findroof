package com.findroof.ethereum;

import android.os.AsyncTask;
import android.text.TextUtils;

import com.findroof.util.Logger;
import com.findroof.util.PropertyService;

import org.spongycastle.util.encoders.Hex;
import org.web3j.crypto.WalletUtils;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.Web3jFactory;
import org.web3j.protocol.core.methods.response.Web3ClientVersion;
import org.web3j.protocol.http.HttpService;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;

public class WalletCreationService {

    public void run(String password, String walletStoreDirectory, WalletCreateListener walletCreateListener) {
        new CreateWallet(walletCreateListener).execute(password, walletStoreDirectory);
    }

    public interface WalletCreateListener {
        void walletCreated();
        void walletCreationFailed();
    }

    private class CreateWallet extends AsyncTask<String, Void, String> {

        WalletCreateListener walletCreateListener;

        public CreateWallet(WalletCreateListener walletCreateListener) {
            this.walletCreateListener = walletCreateListener;
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                final MessageDigest md = MessageDigest.getInstance("SHA-256");
                md.update(params[0].getBytes("UTF-8"));
                final byte[] digest = md.digest();
                final String hashedPassword = Hex.toHexString(digest);

                return WalletUtils.generateNewWalletFile(
                        hashedPassword,
                        new File(params[1]));
            } catch (Exception exception) {
                Logger.error("Unable to create wallet file", exception);
                return "";
            }
        }

        @Override
        protected void onPostExecute(String walletFileName) {
            Logger.debug("walletFile: " + walletFileName);
            PropertyService.getInstance().walletCreated(walletFileName);
            if(TextUtils.isEmpty(walletFileName)) {
                walletCreateListener.walletCreationFailed();
            } else {
                walletCreateListener.walletCreated();
            }
        }
    }

    private String bin2hex(byte[] data) {
        return String.format("%0" + (data.length * 2) + 'x', new BigInteger(1, data));
    }


    private class Web3JRequest extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... params) {
            try {
                final Web3j web3j = Web3jFactory.build(new HttpService("http://192.168.1.6:8545"));
                Web3ClientVersion web3ClientVersion = web3j.web3ClientVersion().send();
                return web3ClientVersion.getWeb3ClientVersion();
            } catch (IOException e) {
                Logger.error("Unable to get client version");
            }

            return "";
        }

        @Override
        protected void onPostExecute(String clientVersion) {
            Logger.debug("Client version is: "+clientVersion);
        }
    }

}
