package com.findroof;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.findroof.fragment.ProgressDialogFragment;
import com.findroof.util.Logger;
import com.findroof.util.PropertyService;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.web3j.protocol.Web3j;
import org.web3j.protocol.Web3jFactory;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.methods.request.Transaction;
import org.web3j.protocol.core.methods.response.EthGetTransactionCount;
import org.web3j.protocol.core.methods.response.EthGetTransactionReceipt;
import org.web3j.protocol.core.methods.response.EthSendTransaction;
import org.web3j.protocol.http.HttpService;
import org.web3j.protocol.parity.Parity;
import org.web3j.protocol.parity.ParityFactory;
import org.web3j.protocol.parity.methods.response.PersonalUnlockAccount;
import org.web3j.utils.Convert;

import java.math.BigInteger;
import java.util.concurrent.ExecutionException;

public class GovernmentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_government);

        findViewById(R.id.rentRules).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent ejariRulesActivityIntent = new Intent(GovernmentActivity.this, GovernmentSetEjariRulesActivity.class);
                startActivity(ejariRulesActivityIntent);
            }
        });

        if(PropertyService.getInstance().getEjariRulesContractAddress() == null) {
            showProgressDialog("Publishing ejari rules contract");
            new PublishEjariRulesContract().execute();
        }

        final FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        final DatabaseReference landlordPNID = firebaseDatabase.getReference("landlordPNID");
        landlordPNID.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final String landlordPNID = dataSnapshot.getValue(String.class);
                Logger.debug("Landlord PNID recvd: "+landlordPNID);
                PropertyService.getInstance().setLandlordPushNotification(landlordPNID);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Logger.error("Failed to read landlord pnid: "+databaseError.getMessage());
            }
        });
    }

    @Override
    protected void onStop() {
        dismissProgressDialog();

        super.onStop();
    }

    private void showProgressDialog(String message) {
        final ProgressDialogFragment submitProgress = new ProgressDialogFragment();
        final Bundle args = new Bundle();
        args.putString("message", message);

        submitProgress.setArguments(args);
        final FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(submitProgress, "progressFrag")
                .commit();
    }

    private void dismissProgressDialog() {
        final Fragment progressFrag = getSupportFragmentManager().findFragmentByTag("progressFrag");
        if(progressFrag != null) {
            ((ProgressDialogFragment) progressFrag).dismiss();
        }
    }

    private class PublishEjariRulesContract extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                final Parity parity = ParityFactory.build(new HttpService(PropertyService.GETH_CLIENT_URL));
                final PersonalUnlockAccount personalUnlockAccount =
                        parity.personalUnlockAccount(PropertyService.GOVERNMENT_ADDRESS, PropertyService.GOVERNMENT_PASSWORD).sendAsync().get();
                if (personalUnlockAccount.accountUnlocked()) {
                    Logger.debug("account unlocked");

                    final Web3j web3j = Web3jFactory.build(new HttpService(PropertyService.GETH_CLIENT_URL));
                    final EthGetTransactionCount ethGetTransactionCount = web3j.ethGetTransactionCount(
                            PropertyService.GOVERNMENT_ADDRESS, DefaultBlockParameterName.LATEST).sendAsync().get();

                    final BigInteger nonce = ethGetTransactionCount.getTransactionCount();
                    final BigInteger gasPrice = Convert.toWei("0.02", Convert.Unit.SZABO).toBigInteger();
                    final BigInteger gasLimit = new BigInteger("4150000");

                    //final List<Type> constructorValues = new ArrayList<>();

                    //final String encodedConstructor =
                    //        FunctionEncoder.encodeConstructor(constructorValues);
                    final Transaction transaction = Transaction.createContractTransaction(
                            PropertyService.GOVERNMENT_ADDRESS,
                            nonce,
                            gasPrice,
                            gasLimit,
                            new BigInteger("0"),
                            "0x6060604052600080546c0100000000000000000000000033810204600160a060020a0319909116179055610320806100376000396000f3606060405260e060020a60003504635610486181146100295780636ed40f9d146100e4575b610002565b346100025761022f6004808035906020019082018035906020019191908080601f01602080910402602001604051908101604052809392919081815260200183838082843750506040805160208835808b0135601f81018390048302840183019094528383529799986044989297509190910194509092508291508401838280828437509496505093359350506064359150506000543373ffffffffffffffffffffffffffffffffffffffff90811691161461024557610002565b34610002576102316004808035906020019082018035906020019191908080601f01602080910402602001604051908101604052809392919081815260200183838082843750506040805160208835808b0135601f81018390048302840183019094528383529799986044989297509190910194509092508291508401838280828437509496505093359350506064359150506000600060006001600050600060028989600060405160200152604051808380519060200190808383829060006004602084601f0104600302600f01f1509050018280519060200190808383829060006004602084601f0104600302600f01f150905001925050506020604051808303816000866161da5a03f11561000257505060408051518252602082019290925201600020805460018201549193506064908101870204915084111561030f5760009250610305565b005b604080519115158252519081900360200190f35b604060405190810160405280838152602001828152602001506001600050600060028787600060405160200152604051808380519060200190808383829060006004602084601f0104600302600f01f1509050018280519060200190808383829060006004602084601f0104600302600f01f150905001925050506020604051808303816000866161da5a03f11561000257505060408051518252602082810193909352016000208251815591015160019091015550505050565b600192505b5050949350505050565b80841115610300576000925061030556"
                                    );

                    final EthSendTransaction transactionResponse =
                            parity.ethSendTransaction(transaction).sendAsync().get();

                    final String transactionHash = transactionResponse.getTransactionHash();
                    if (transactionResponse.getError() != null) {

                        Logger.debug("tx error msg: " + transactionResponse.getError().getMessage() + ", reason: " + transactionResponse.getError().getData());
                    }
                    Logger.debug("transaction hash:  " + transactionHash);

                    if (transactionHash != null) {

                        EthGetTransactionReceipt transactionReceipt;
                        do {
                            Logger.debug("getting receipt");
                            transactionReceipt = web3j.ethGetTransactionReceipt(transactionHash).sendAsync().get();//.getTransactionReceipt();
                            Thread.sleep(10000);
                        } while (transactionReceipt.getTransactionReceipt() == null);

                        if (transactionReceipt.hasError()) {
                            Logger.debug("txReceipt error msg: " + transactionReceipt.getError().getMessage() + ", reason: " + transactionReceipt.getError().getData());
                        }
                        if (transactionReceipt.getTransactionReceipt() != null) {
                            Logger.debug("contract address: " + transactionReceipt.getTransactionReceipt().getContractAddress());
                            PropertyService.getInstance().setEjariRulesContractAddress(transactionReceipt.getTransactionReceipt().getContractAddress());
                        }

                        return true;
                    }

                    return false;
                } else {
                    return false;
                }
            } catch (InterruptedException exception) {
                Logger.error("Unable to create contract", exception);
            } catch (ExecutionException exception) {
                Logger.error("Unable to create contract", exception);
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            dismissProgressDialog();
            if(result) {
                Toast.makeText(GovernmentActivity.this, "Ejari Rules contract created", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(GovernmentActivity.this, "Unable to create Ejari Rules Contract", Toast.LENGTH_LONG).show();
            }
        }
    }

}
