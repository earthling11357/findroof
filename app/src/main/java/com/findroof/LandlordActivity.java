package com.findroof;

import android.content.Intent;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.NfcEvent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.findroof.util.Logger;
import com.findroof.util.PropertyService;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONException;
import org.json.JSONObject;

import static android.nfc.NdefRecord.createMime;

public class LandlordActivity extends AppCompatActivity implements NfcAdapter.CreateNdefMessageCallback {

    private NfcAdapter mNfcAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landlord);

        ((Button) findViewById(R.id.syncWithTenant)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mNfcAdapter = NfcAdapter.getDefaultAdapter(LandlordActivity.this);
                if (mNfcAdapter == null) {
                    Toast.makeText(LandlordActivity.this, "NFC is not available", Toast.LENGTH_LONG).show();
                    finish();
                    return;
                }
                // Register callback
                mNfcAdapter.setNdefPushMessageCallback(LandlordActivity.this, LandlordActivity.this);
            }
        });

        final FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        final DatabaseReference governmentPNID = firebaseDatabase.getReference("governmentPNID");
        governmentPNID.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final String governmentPNID = dataSnapshot.getValue(String.class);
                Logger.debug("Govt PNID recvd: "+governmentPNID);
                PropertyService.getInstance().setGovernmentPushNotificationId(governmentPNID);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Logger.error("Failed to read govt pnid: "+databaseError.getMessage());
            }
        });

        final DatabaseReference tenantPNID = firebaseDatabase.getReference("tenantPNID");
        tenantPNID.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final String tenantPNID = dataSnapshot.getValue(String.class);
                Logger.debug("Tenant PNID recvd: "+tenantPNID);
                PropertyService.getInstance().setTenantPushNotificationId(tenantPNID);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Logger.error("Failed to read tenant pnid: "+databaseError.getMessage());
            }
        });

        if(PropertyService.getInstance().getLandlordName() != null) {
            findViewById(R.id.nameWrapper).setVisibility(View.GONE);
            findViewById(R.id.saveName).setVisibility(View.GONE);
        } else {
            findViewById(R.id.saveName).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PropertyService.getInstance().setLandlordName(
                            ((TextInputEditText) findViewById(R.id.name)).getText().toString());
                    findViewById(R.id.nameWrapper).setVisibility(View.GONE);
                    findViewById(R.id.saveName).setVisibility(View.GONE);
                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if(PropertyService.getInstance().getHouseContractAddress() == null) {
            findViewById(R.id.updateRent).setVisibility(View.GONE);
        } else {
            findViewById(R.id.updateRent).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Intent updateContractRentActivityIntent = new Intent(LandlordActivity.this, UpdateContractRentActivity.class);
                    startActivity(updateContractRentActivityIntent);
                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_house_contract, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.addHouseContract:
                final Intent createHouseActivityIntent = new Intent(this, CreateHouseContractActivity.class);
                startActivity(createHouseActivityIntent);

                return true;
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public NdefMessage createNdefMessage(NfcEvent event) {
        Logger.debug("Creating ndef msg");

        final NdefMessage msg;
        try {
            PropertyService propertyService = PropertyService.getInstance();
            final JSONObject dataObject = new JSONObject();
            dataObject.put("houseContractAddress", propertyService.getHouseContractAddress());

            final String text = dataObject.toString();
            msg = new NdefMessage(
                    new NdefRecord[] {
                            createMime("application/vnd.com.findroof.android.beam",
                                    text.getBytes()
                            ),
                            NdefRecord.createApplicationRecord("com.findroof")
                    });

            return msg;
        } catch (JSONException exception) {
            Logger.error("Unable to create json payload", exception);
            return null;
        }
    }
}
