package com.findroof;

public enum UsageMode {

    GOVERNMENT(1),
    TENANT(2),
    LANDLORD(3);

    int mode;

    UsageMode(int mode) {
        this.mode = mode;
    }

    public int mode() {
        return mode;
    }

    public static UsageMode fromMode(int mode) {
        switch (mode) {
            case 1:
                return UsageMode.GOVERNMENT;
            case 2:
                return UsageMode.TENANT;
            case 3:
                return UsageMode.LANDLORD;
            default:
                return null;
        }
    }

}
