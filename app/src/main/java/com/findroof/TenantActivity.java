package com.findroof;

import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.findroof.util.Logger;
import com.findroof.util.PropertyService;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class TenantActivity extends AppCompatActivity {

    private NfcAdapter mNfcAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tenant);

        final FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        final DatabaseReference landlordPNID = firebaseDatabase.getReference("landlordPNID");
        landlordPNID.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final String landlordPNID = dataSnapshot.getValue(String.class);
                Logger.debug("Landlord PNID recvd: "+landlordPNID);
                PropertyService.getInstance().setLandlordPushNotification(landlordPNID);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Logger.error("Failed to read landlord pnid: "+databaseError.getMessage());
            }
        });
    }


}
