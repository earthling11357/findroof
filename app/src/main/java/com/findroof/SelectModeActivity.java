package com.findroof;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.findroof.fragment.ProgressDialogFragment;
import com.findroof.util.Logger;
import com.findroof.util.PropertyService;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

import static com.findroof.UsageMode.GOVERNMENT;
import static com.findroof.UsageMode.LANDLORD;
import static com.findroof.UsageMode.TENANT;

public class SelectModeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_mode);

        final PropertyService propertyService = PropertyService.getInstance();
        final UsageMode usageMode = propertyService.getUsageMode();
        if(usageMode != null) {
            switch (usageMode) {
                case GOVERNMENT:
                    startActivity(new Intent(this, GovernmentActivity.class));
                    break;
                case TENANT:
                    startActivity(new Intent(this, TenantActivity.class));
                    break;
                case LANDLORD:
                    startActivity(new Intent(this, LandlordActivity.class));
                    break;
            }

            //end self
            finish();
        }

        final String[] options = {"Government", "Tenant", "Landlord"};
        final ArrayAdapter<String> optionsAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, options);
        final ListView modeList = (ListView) findViewById(R.id.modeList);
        modeList.setAdapter(optionsAdapter);
        modeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final ProgressDialogFragment submitProgress = new ProgressDialogFragment();
                final Bundle args = new Bundle();
                args.putString("message", "Initializing please wait...");

                submitProgress.setArguments(args);
                final FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.add(submitProgress, "progressFrag")
                        .commit();

                final UsageMode selectedUsageMode = UsageMode.fromMode(position + 1);
                propertyService.setUsageMode(selectedUsageMode);

                final String pushNotificationId = FirebaseInstanceId.getInstance().getToken();
                Logger.debug("push notification Id: "+pushNotificationId);
                final FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
                switch (selectedUsageMode) {
                    case GOVERNMENT: {
                        propertyService.setUsageMode(GOVERNMENT);

                        final DatabaseReference governmentPNID = firebaseDatabase.getReference("governmentPNID");
                        governmentPNID.setValue(pushNotificationId);

                        startActivity(new Intent(SelectModeActivity.this, GovernmentActivity.class));
                        break;
                    }
                    case TENANT: {
                        propertyService.setUsageMode(TENANT);

                        final DatabaseReference tenantPNID = firebaseDatabase.getReference("tenantPNID");
                        tenantPNID.setValue(pushNotificationId);

                        startActivity(new Intent(SelectModeActivity.this, TenantActivity.class));
                        break;
                    }
                    case LANDLORD: {
                        propertyService.setUsageMode(LANDLORD);

                        final DatabaseReference landlordPNID = firebaseDatabase.getReference("landlordPNID");
                        landlordPNID.setValue(pushNotificationId);

                        startActivity(new Intent(SelectModeActivity.this, LandlordActivity.class));
                        break;
                    }
                }

                finish();
                dismissProgressDialog();
            }
        });
    }

    @Override
    protected void onStop() {
        dismissProgressDialog();
        super.onStop();
    }

    private void dismissProgressDialog() {
        final Fragment progressFrag = getSupportFragmentManager().findFragmentByTag("progressFrag");
        if(progressFrag != null) {
            ((ProgressDialogFragment) progressFrag).dismiss();
        }
    }

}

