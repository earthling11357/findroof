package com.findroof;

import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.findroof.fragment.ProgressDialogFragment;
import com.findroof.util.HttpUtils;
import com.findroof.util.Logger;
import com.findroof.util.PropertyService;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.web3j.abi.FunctionEncoder;
import org.web3j.abi.datatypes.Type;
import org.web3j.abi.datatypes.Utf8String;
import org.web3j.abi.datatypes.generated.Uint256;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.Web3jFactory;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.methods.request.Transaction;
import org.web3j.protocol.core.methods.response.EthGetTransactionCount;
import org.web3j.protocol.core.methods.response.EthGetTransactionReceipt;
import org.web3j.protocol.core.methods.response.EthSendTransaction;
import org.web3j.protocol.http.HttpService;
import org.web3j.protocol.parity.Parity;
import org.web3j.protocol.parity.ParityFactory;
import org.web3j.protocol.parity.methods.response.PersonalUnlockAccount;
import org.web3j.utils.Convert;

import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class CreateHouseContractActivity extends AppCompatActivity implements OnMapReadyCallback,
        GoogleMap.OnMapClickListener {

    private GoogleMap map;
    private Marker houseMarker;
    private LatLng selectedLatLng;
    private boolean resolvingAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_house_contract);

        final SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapClick(LatLng latLng) {
        Logger.debug("selected latlng are: "+latLng.latitude+", "+latLng.longitude);
        resolvingAddress = true;

        final ProgressDialogFragment submitProgress = new ProgressDialogFragment();
        final Bundle args = new Bundle();
        args.putString("message", "Resolving address...");

        submitProgress.setArguments(args);
        final FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(submitProgress, "progressFrag")
                .commit();

        selectedLatLng = latLng;
        if(houseMarker != null) {
            houseMarker.remove();
        }

        houseMarker = map.addMarker(new MarkerOptions()
                            .position(latLng)
                            .title("House"));

        new GetAddressTask().execute(latLng);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.map = googleMap;
        googleMap.setOnMapClickListener(this);
    }

    @Override
    protected void onStop() {
        dismissProgressDialog();

        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.create_house_contract, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.createHouseContract:
                if(resolvingAddress) {
                    Toast.makeText(this, "Please wait while the address is resolving", Toast.LENGTH_LONG).show();
                    return true;
                }

                ((TextInputLayout) findViewById(R.id.rentWrapper)).setErrorEnabled(false);
                ((TextInputLayout) findViewById(R.id.securityWrapper)).setErrorEnabled(false);

                final ProgressDialogFragment submitProgress = new ProgressDialogFragment();
                final Bundle args = new Bundle();
                args.putString("message", "Creating contract...");

                submitProgress.setArguments(args);
                final FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.add(submitProgress, "progressFrag")
                        .commit();

                new CreateContractTask(new ContractInput(selectedLatLng,
                        ((TextInputEditText) findViewById(R.id.rent)).getText().toString(),
                        ((TextInputEditText) findViewById(R.id.security)).getText().toString(),
                        (String) ((Spinner) findViewById(R.id.propertyType)).getSelectedItem(),
                        (String) ((Spinner) findViewById(R.id.propertyType)).getSelectedItem())).execute();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void dismissProgressDialog() {
        final Fragment progressFrag = getSupportFragmentManager().findFragmentByTag("progressFrag");
        if(progressFrag != null) {
            ((ProgressDialogFragment) progressFrag).dismiss();
        }
    }

    private class GetAddressTask extends AsyncTask<LatLng, Void, Address> {

        @Override
        protected Address doInBackground(LatLng... params) {
            try {
                final LatLng location = params[0];
                final Geocoder geocoder = new Geocoder(CreateHouseContractActivity.this, Locale.US);
                final List<Address> addresses = geocoder.getFromLocation(
                                                    location.latitude,
                                                    location.longitude, 1);
                if(addresses != null && !addresses.isEmpty()) {
                    return addresses.get(0);
                }
            } catch (IOException exception) {
                Logger.error("Unable to get addresses", exception);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Address address) {
            String addressText = "Unable to resolve address";
            if(address != null) {
                Logger.debug(address.toString());
                addressText = "";
                for (int i = 0; i <= address.getMaxAddressLineIndex(); i++) {
                    addressText = addressText + address.getAddressLine(i);
                    if(i != address.getMaxAddressLineIndex()) {
                        addressText = addressText + "\n";
                    }
                }
            }

            ((TextView) findViewById(R.id.houseAddress)).setText(addressText);
            dismissProgressDialog();
            resolvingAddress = false;
        }
    }

    class ContractInput {
        LatLng latLng;
        String rent;
        String securityDeposit;
        String propertyType;
        String bedroomType;

        public ContractInput(LatLng latLng, String rent, String securityDeposit, String propertyType, String bedroomType) {
            this.latLng = latLng;
            this.rent = rent;
            this.securityDeposit = securityDeposit;
            this.propertyType = propertyType;
            this.bedroomType = bedroomType;
        }
    }

    private class CreateContractTask extends AsyncTask<Void, Void, Boolean> {

        final ContractInput contractInput;

        CreateContractTask(ContractInput contractInput) {
            this.contractInput = contractInput;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                final Parity parity = ParityFactory.build(new HttpService(PropertyService.GETH_CLIENT_URL));
                final PersonalUnlockAccount personalUnlockAccount =
                        parity.personalUnlockAccount(PropertyService.LANDLORD_ADDRESS, PropertyService.LANDLORD_PASSWORD).sendAsync().get();
                if(personalUnlockAccount.accountUnlocked()) {
                    Logger.debug("account unlocked");

                    final Web3j web3j = Web3jFactory.build(new HttpService(PropertyService.GETH_CLIENT_URL));
                    final EthGetTransactionCount ethGetTransactionCount = web3j.ethGetTransactionCount(
                            PropertyService.LANDLORD_ADDRESS, DefaultBlockParameterName.LATEST).sendAsync().get();

                    final BigInteger nonce = ethGetTransactionCount.getTransactionCount();
                    final BigInteger gasPrice = Convert.toWei("0.02", Convert.Unit.SZABO).toBigInteger();
                    final BigInteger gasLimit = new BigInteger("4150000");

                    final Utf8String _latitude = new Utf8String(Double.toString(contractInput.latLng.latitude));
                    final Utf8String _longitude = new Utf8String(Double.toString(contractInput.latLng.longitude));

                    final Uint256 _rent = new Uint256(Convert.toWei(contractInput.rent, Convert.Unit.SZABO).toBigInteger());
                    final Uint256 _security = new Uint256(Convert.toWei(contractInput.securityDeposit, Convert.Unit.SZABO).toBigInteger());

                    final List<Type> constructorValues = new ArrayList<>();
                    constructorValues.add(_latitude);
                    constructorValues.add(_longitude);
                    constructorValues.add(_rent);
                    constructorValues.add(_security);

                    final String encodedConstructor =
                            FunctionEncoder.encodeConstructor(constructorValues);
                    final Transaction transaction = Transaction.createContractTransaction(
                            PropertyService.LANDLORD_ADDRESS,
                            nonce,
                            gasPrice,
                            gasLimit,
                            new BigInteger("0"),
                            "0x6000805460a060020a60ff0219600160a060020a03199091167314a04ff8ac187a6a9454550ab9b04f97dbc3373817169055602a6060819052600a6080819052908155600b81905560e06040819052602560a081905260c0839052600c55600d91909155610ad938819003908190833981016040528051610100516101205161014051928401939190910191600380546c0100000000000000000000000033810204600160a060020a0319909116179055835160018054600082905290916020601f60026000198587161561010002019094169390930483018190047fb10e2d527612073b26eecdfd717e6a320cf44b4afac2b0732d9fcbe2b7fa0cf690810193909189019083901061013557805160ff19168380011785555b506101659291505b808211156101be5760008155600101610121565b82800160010185558215610119579182015b82811115610119578251826000505591602001919060010190610147565b50508260026000509080519060200190828054600181600116156101000203166002900490600052602060002090601f016020900481019282601f106101c257805160ff19168380011785555b506101f2929150610121565b5090565b828001600101855582156101b2579182015b828111156101b25782518260005055916020019190600101906101d4565b50506004829055600581905560035460005460408051600160a060020a03938416815292909116602083015280517f0a31ee9d46a828884b81003c8498156ea6aa15b9b54bdd0ef0b533d9eba57e559281900390910190a15050505061087d8061025c6000396000f3606060405236156100cf5760e060020a600035046306096b6981146100d45780630f4b1cfc146100e557806339c0ea6e1461011257806355b72f38146101385780635bae4e98146101a45780636758a1a0146101c45780636901f668146101d55780636e9c4d0a146101f8578063750a4937146102215780637817abee146102a15780637a828b28146102b457806383fcf308146102d557806388968b94146102f5578063ae7b6bd91461031b578063e690833a14610389578063eb9220ab1461039c578063ef48eee6146103c2575b610002565b34610002576103de600c54600d5482565b346100025760008054600160a060020a0319166c010000000000000000000000006004358102041790555b005b346100025761011060043560065433600160a060020a0390811691161461054457610002565b346100025760408051602080820183526000825260028054845160018216156101000260001901909116829004601f81018490048402820184019095528481526103f79490928301828280156105835780601f1061055857610100808354040283529160200191610583565b3461000257610465600b54600a546000919081156100025704905061058b565b34610002576103de600a54600b5482565b346100025761011060005433600160a060020a0390811691161461058e57610002565b346100025761011060043560035460009033600160a060020a0390811691161461062c57610002565b34610002576104776040805160208181018352600080835283518083018552908152600180548551601f6002600019610100858716150201909316839004908101869004860282018601909752868152949592949193909284918301828280156106885780601f1061065d57610100808354040283529160200191610688565b34610002576103de600a54600b545b9091565b61011060043560035433600160a060020a0390811691161461072657610002565b3461000257610465600d54600c546000919081156100025704905061058b565b346100025761011060043560065433600160a060020a039081169116146107ad57610002565b3461000257604080516020808201835260008252600180548451600282841615610100026000190190921691909104601f81018490048402820184019095528481526103f79490928301828280156105835780601f1061055857610100808354040283529160200191610583565b34610002576103de600c54600d546102b0565b346100025761011060043560035433600160a060020a039081169116146107c157610002565b610110600435602435600554600454013410156107d557610002565b6040805192835260208301919091528051918290030190f35b60405180806020018281038252838181518152602001915080519060200190808383829060006004602084601f0104600302600f01f150905090810190601f1680156104575780820380516001836020036101000a031916815260200191505b509250505060405180910390f35b60408051918252519081900360200190f35b6040518080602001806020018381038352858181518152602001915080519060200190808383829060006004602084601f0104600302600f01f150905090810190601f1680156104db5780820380516001836020036101000a031916815260200191505b508381038252848181518152602001915080519060200190808383829060006004602084601f0104600302600f01f150905090810190601f1680156105345780820380516001836020036101000a031916815260200191505b5094505050505060405180910390f35b600c805482019055600d8054600101905550565b820191906000526020600020905b81548152906001019060200180831161056657829003601f168201915b505050505090505b90565b60005433600160a060020a039081169116146105a957610002565b6000805474ff0000000000000000000000000000000000000000191674010000000000000000000000000000000000000000179081905560035460408051600160a060020a03938416815291909216602082015281517fd6c06c253ccd3a2c9b0c848536c88ce68a82ec904b839430912363c1f5be321c929181900390910190a1565b50600654600160a060020a031660009081526009602052604090208054820181556001818101805490910190555050565b820191906000526020600020905b81548152906001019060200180831161066b57829003601f168201915b5050845460408051602060026001851615610100026000190190941693909304601f8101849004840282018401909252818152959750869450925084019050828280156107165780601f106106eb57610100808354040283529160200191610716565b820191906000526020600020905b8154815290600101906020018083116106f957829003601f168201915b50505050509050915091506102b0565b600654600554604051600160a060020a03909216919083900380156108fc02916000818181858888f1935050505080156107855750600354604051600160a060020a039091169082156108fc029083906000818181858888f193505050505b151561079057610002565b60068054600160a060020a03191690556000600781905560085550565b600a805482019055600b8054600101905550565b6008544210156107d057610002565b600455565b600354600454604051600160a060020a039092169181156108fc0291906000818181858888f19350505050151561080b57610002565b60068054600160a060020a0319166c0100000000000000000000000033810204179081905560035460408051600160a060020a03938416815292909116602083015280517f36c4359614c37ef31afa649625f064e2ca57f0faa6dbc8ca2548356e23d944bc9281900390910190a1505056"
                                   + encodedConstructor);

                    final EthSendTransaction transactionResponse =
                            parity.ethSendTransaction(transaction).sendAsync().get();

                    final String transactionHash = transactionResponse.getTransactionHash();
                    if(transactionResponse.getError() != null) {

                        Logger.debug("tx error msg: "+transactionResponse.getError().getMessage()+", reason: "+transactionResponse.getError().getData());
                    }
                    Logger.debug("transaction hash:  " +transactionHash);

                    if(transactionHash != null) {

                        EthGetTransactionReceipt transactionReceipt;
                        do {
                            Logger.debug("getting receipt");
                            transactionReceipt = web3j.ethGetTransactionReceipt(transactionHash).sendAsync().get();//.getTransactionReceipt();
                            Thread.sleep(10000);
                        } while(transactionReceipt.getTransactionReceipt() == null);

                        if(transactionReceipt.hasError()) {
                            Logger.debug("txReceipt error msg: "+transactionReceipt.getError().getMessage()+", reason: "+transactionReceipt.getError().getData());
                        }
                        if(transactionReceipt.getTransactionReceipt() != null) {
                            Logger.debug("contract address: " + transactionReceipt.getTransactionReceipt().getContractAddress());
                            PropertyService propertyService = PropertyService.getInstance();
                            propertyService.setHouseContractAddress(transactionReceipt.getTransactionReceipt().getContractAddress());

                            final Map<String, String> pushParams = new HashMap<>();
                            pushParams.put("component", "new_property");
                            pushParams.put("latitude", Double.toString(contractInput.latLng.latitude));
                            pushParams.put("longitude", Double.toString(contractInput.latLng.longitude));
                            pushParams.put("landlordName", propertyService.getLandlordName());
                            pushParams.put("propertyType", contractInput.propertyType);
                            pushParams.put("bedroomType", contractInput.bedroomType);
                            pushParams.put("contractAddress", transactionReceipt.getTransactionReceipt().getContractAddress());

                            new HttpUtils().pushMessageAsync(propertyService.getGovernmentPushNotificationId(), pushParams, new HttpUtils.RequestListenerAdapter<String>() {
                                @Override
                                public void onError() {

                                }

                                @Override
                                public void noNetworkConnection() {

                                }
                            });
                        }

                        return true;
                    }

                    return false;
                } else {
                    return false;
                }

                /*final Credentials credentials = WalletUtils.loadCredentials(
                        "9b56ca8566a48b98a8c29a7fd307038ed555123439a937eb85d9c45166881e6e",
                        new File(getExternalFilesDir(null), "landlord.json").getAbsolutePath()
                );*/








                /*final byte[] signedMessage = TransactionEncoder.signMessage(rawTransaction, credentials);
                final String signedMessageHexValue = Hex.toHexString(signedMessage);
                final EthSendTransaction ethSendTransaction = web3j.ethSendRawTransaction(signedMessageHexValue).sendAsync().get();
                */


                //return true;
            } catch (InterruptedException exception) {
                Logger.error("Unable to create contract", exception);
            } catch (ExecutionException exception) {
                Logger.error("Unable to create contract", exception);
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            dismissProgressDialog();
            if(result) {
                Toast.makeText(CreateHouseContractActivity.this, "Contract created. Waiting for government to approve it", Toast.LENGTH_LONG).show();
                finish();
            } else {
                Toast.makeText(CreateHouseContractActivity.this, "Unable to create Contract", Toast.LENGTH_LONG).show();
            }
        }

    }
}
