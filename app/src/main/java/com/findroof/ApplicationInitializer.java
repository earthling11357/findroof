package com.findroof;

import android.support.multidex.MultiDexApplication;

import com.findroof.util.HttpUtils;
import com.findroof.util.PRNGFixes;
import com.findroof.util.PropertyService;



/**
 * initialises application-wide objects when the process for this application
 * is created.
 * <p>
 * This class is referenced in android:name attribute of application tag in Manifest.
 */
public final class ApplicationInitializer extends MultiDexApplication {

    @Override
    public void onCreate() {
        super.onCreate();

        init();
    }

    private void init() {

        //apply PRNG fixes.
        //For more info read: http://android-developers.blogspot.in/2013/08/some-securerandom-thoughts.html
        PRNGFixes.apply();

        //initialize PropertyService
        PropertyService.init(this);

        //init http utils
        HttpUtils.init(getApplicationContext());
    }

}
