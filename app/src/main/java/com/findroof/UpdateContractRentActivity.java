package com.findroof;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.findroof.fragment.ProgressDialogFragment;
import com.findroof.util.Logger;
import com.findroof.util.PropertyService;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.web3j.abi.FunctionEncoder;
import org.web3j.abi.FunctionReturnDecoder;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Function;
import org.web3j.abi.datatypes.Type;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.Web3jFactory;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.methods.request.Transaction;
import org.web3j.protocol.core.methods.response.EthCall;
import org.web3j.protocol.http.HttpService;
import org.web3j.protocol.parity.Parity;
import org.web3j.protocol.parity.ParityFactory;
import org.web3j.protocol.parity.methods.response.PersonalUnlockAccount;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class UpdateContractRentActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap map;
    private ContractDetails contractDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_contract_rent);

        final SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        showProgressDialog("Getting property details...");
        new GetContractDetails(PropertyService.getInstance().getHouseContractAddress()).execute();
    }

    private void populateDetails() {
        Logger.debug("contract details: "+contractDetails.toString());
        map.addMarker(new MarkerOptions()
                .position(new LatLng(Double.parseDouble(contractDetails.latitude),
                        Double.parseDouble(contractDetails.longitude)))
                .title("House"));

        ((TextView) findViewById(R.id.rent)).setText("200");
        ((TextView) findViewById(R.id.securityDeposit)).setText("200");
        //((TextView) findViewById(R.id.rent)).setText(Convert.fromWei(contractDetails.rent.toString(), Convert.Unit.SZABO).toBigInteger().toString());
        //((TextView) findViewById(R.id.securityDeposit)).setText(Convert.fromWei(contractDetails.rent.toString(), Convert.Unit.SZABO).toBigInteger().toString());
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.map = googleMap;
    }

    @Override
    protected void onStop() {
        dismissProgressDialog();

        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.update_property_rent, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.updateProperty) {
            showProgressDialog("Updating rent");
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... params) {
                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException e) {
                        Logger.error("", e);
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    dismissProgressDialog();
                    Toast.makeText(UpdateContractRentActivity.this, "Unable to update rent as it exceeds the rule for this place", Toast.LENGTH_LONG).show();
                }
            }.execute();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showProgressDialog(String message) {
        final ProgressDialogFragment submitProgress = new ProgressDialogFragment();
        final Bundle args = new Bundle();
        args.putString("message", message);

        submitProgress.setArguments(args);
        final FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(submitProgress, "progressFrag")
                .commit();
    }

    private void dismissProgressDialog() {
        final Fragment progressFrag = getSupportFragmentManager().findFragmentByTag("progressFrag");
        if(progressFrag != null) {
            ((ProgressDialogFragment) progressFrag).dismiss();
        }
    }

    private class ContractDetails {
        String latitude;
        String longitude;
        String landlordAvgRating;
        String propertyAvgRating;
        BigInteger rent;
        BigInteger security;

        @Override
        public String toString() {
            return "ContractDetails{" +
                    "latitude='" + latitude + '\'' +
                    ", longitude='" + longitude + '\'' +
                    ", landlordAvgRating='" + landlordAvgRating + '\'' +
                    ", propertyAvgRating='" + propertyAvgRating + '\'' +
                    ", rent=" + rent +
                    ", security=" + security +
                    '}';
        }

        public ContractDetails(String latitude, String longitude, String landlordAvgRating, String propertyAvgRating, BigInteger rent, BigInteger security) {
            this.latitude = latitude;
            this.longitude = longitude;
            this.landlordAvgRating = landlordAvgRating;
            this.propertyAvgRating = propertyAvgRating;
            this.rent = rent;
            this.security = security;
        }
    }

    private class GetContractDetails extends AsyncTask<Void, Void, ContractDetails> {

        String contractAddress;

        GetContractDetails(String contractAddress) {
            this.contractAddress = contractAddress;
        }

        @Override
        protected ContractDetails doInBackground(Void... params) {
            try {
                final Parity parity = ParityFactory.build(new HttpService(PropertyService.GETH_CLIENT_URL));
                final PersonalUnlockAccount personalUnlockAccount =
                        parity.personalUnlockAccount(PropertyService.TENANT_ADDRESS, PropertyService.TENANT_PASSWORD).sendAsync().get();
                if(personalUnlockAccount.accountUnlocked()) {
                    Logger.debug("account unlocked");

                    final Web3j web3j = Web3jFactory.build(new HttpService(PropertyService.GETH_CLIENT_URL));

                    Logger.debug("get coordinates");
                    final List<Type> functionParams = new ArrayList<>();
                    final List<TypeReference<?>> functionParamTypeRefs = new ArrayList<>();
                    Function function = new Function("getLongitude",
                            functionParams, functionParamTypeRefs);

                    String encodedFunction = FunctionEncoder.encode(function);
                    EthCall response = web3j.ethCall(
                            Transaction.createEthCallTransaction(contractAddress, encodedFunction),
                            DefaultBlockParameterName.LATEST)
                            .sendAsync().get();

                    Logger.debug("val: "+response.getValue()+", result: "+response.getResult());
                    if(response.getError() != null) {
                    Logger.debug("error message:" +response.getError().getMessage());
                    }
                    List<Type> returnTypes = FunctionReturnDecoder.decode(
                            response.getValue(), function.getOutputParameters());

                    for(Type returnType : returnTypes) {
                        Logger.debug(returnType.getValue() + ", " + returnType.getTypeAsString());
                    }

                    Logger.debug("getownerratings");
                    function = new Function("getOwnerRatingTuple",
                            functionParams, functionParamTypeRefs);
                    encodedFunction = FunctionEncoder.encode(function);
                    response = web3j.ethCall(
                            Transaction.createEthCallTransaction(contractAddress, encodedFunction),
                            DefaultBlockParameterName.LATEST)
                            .sendAsync().get();

                    returnTypes = FunctionReturnDecoder.decode(
                            response.getValue(), function.getOutputParameters());

                    for(Type returnType : returnTypes) {
                        Logger.debug(returnType.getValue() + ", " + returnType.getTypeAsString());
                    }

                    Logger.debug("getpropertyratings");
                    function = new Function("getPropertyRatingTuple",
                            functionParams, functionParamTypeRefs);
                    encodedFunction = FunctionEncoder.encode(function);
                    response = web3j.ethCall(
                            Transaction.createEthCallTransaction(contractAddress, encodedFunction),
                            DefaultBlockParameterName.LATEST)
                            .sendAsync().get();

                    returnTypes = FunctionReturnDecoder.decode(
                            response.getValue(), function.getOutputParameters());

                    for(Type returnType : returnTypes) {
                        Logger.debug(returnType.getValue() + ", " + returnType.getTypeAsString());
                    }


                    return null;
                } else {
                    return null;
                }
            } catch (InterruptedException exception) {
                Logger.error("Unable to set contract valid", exception);
            } catch (ExecutionException exception) {
                Logger.error("Unable to set contract valid", exception);
            }
            return null;
        }

        @Override
        protected void onPostExecute(ContractDetails contractDetails) {
            if(contractDetails != null) {
                UpdateContractRentActivity.this.contractDetails = contractDetails;

            } else {
                UpdateContractRentActivity.this.contractDetails =
                        new ContractDetails("25.23363026245991", "55.367910638451576", "4.2", "4.5", new BigInteger("200"), new BigInteger("200"));
            }
            populateDetails();
            dismissProgressDialog();
        }
    }
}
