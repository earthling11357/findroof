package com.findroof.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;

public class NetworkUtils {

    /**
     * Checks whether the device currently has a network connection.
     * @param context
     * @return true if the device has a network connection, false otherwise.
     */
    public static boolean isDeviceOnline(@NonNull final Context context) {
        final ConnectivityManager connMgr =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected() && networkInfo.isAvailable());
    }


}
