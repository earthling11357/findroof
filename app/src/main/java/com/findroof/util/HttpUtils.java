package com.findroof.util;

import android.content.Context;
import android.support.annotation.NonNull;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class HttpUtils {

    private static Context context;

    public static void init(@NonNull final Context context) {
        HttpUtils.context = context;
    }

    public void get(@NonNull final String url, @NonNull final RequestListener<String> requestListener) {
        if(!NetworkUtils.isDeviceOnline(context)) {
            requestListener.noNetworkConnection();
            return;
        }

        final OkHttpClient client = new OkHttpClient();
        final Request request = new Request.Builder()
                .url(url)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException exception) {
                Logger.error("Request returned with error", exception);

                requestListener.onError();
            }

            @Override
            public void onResponse(Call call, Response response) {
                Logger.debug("Request returned with code "+response.code());

                if(response.code() != 200) {
                    requestListener.onError();
                    return;
                }

                final ResponseBody responseBody = response.body();
                try {
                    final String responseBodyString = responseBody.string();

                    requestListener.onSuccess(responseBodyString);
                } catch (IOException exception) {
                    onFailure(call, exception);
                } finally {
                    responseBody.close();
                }
            }
        });
    }

    public void pushMessageAsync(@NonNull String toPushNotificationId,
                                 @NonNull final Map<String, String> parameters,
                          @NonNull final RequestListener<String> requestListener) {
        if(!NetworkUtils.isDeviceOnline(context)) {
            requestListener.noNetworkConnection();
            return;
        }

        final OkHttpClient client = new OkHttpClient.Builder()
                .readTimeout(2, TimeUnit.MINUTES)
                .writeTimeout(5, TimeUnit.MINUTES)
                .retryOnConnectionFailure(false)
                .build();
        final Request request = createPostRequest("https://fcm.googleapis.com/fcm/send", toPushNotificationId, parameters);

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException exception) {
                Logger.error("Request returned with error", exception);

                //Toast.makeText(context, "Exception: "+exception.getMessage(), Toast.LENGTH_LONG).show();
                requestListener.onError();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                Logger.debug("Request returned with code "+response.code());

                if(response.code() >= 200 && response.code() <= 299) {
                    final ResponseBody responseBody = response.body();
                    try {
                        final String responseBodyString = responseBody.string();

                        requestListener.onSuccess(responseBodyString);
                    } catch (IOException exception) {
                        onFailure(call, exception);
                    } finally {
                        responseBody.close();
                    }
                } else {
                    //Toast.makeText(context, "Error code: "+response.code(), Toast.LENGTH_LONG).show();
                    requestListener.onError();
                }
            }
        });
    }

    public interface RequestListener<T> {

        void onSuccess(T response);

        void onSuccess();

        void onError();

        void noNetworkConnection();

    }

    public static abstract class RequestListenerAdapter<T> implements RequestListener<T> {

        @Override
        public void onSuccess(T response) {
            onSuccess();
        }

        @Override
        public void onSuccess() {
            //noop
        }

    }

    private Request createPostRequest(@NonNull String url, @NonNull String toPushNotificationId, @NonNull Map<String, String> parameters) {
        final JSONObject payload = new JSONObject();
        try {
            final JSONObject data = new JSONObject();
            for(Map.Entry<String, String> param : parameters.entrySet()) {
                data.put(param.getKey(), param.getValue());
            }

            payload.put("data", data);
            payload.put("priority", "high");
            payload.put("to", toPushNotificationId);
        } catch (JSONException e) {
            Logger.error("Unable to create push notification payload");
        }

        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, payload.toString());
        return new Request.Builder()
                .url(url)
                .addHeader("Authorization", "key="+PropertyService.SERVER_PUSH_NOTIFICATION_KEY)
                .addHeader("Content-Type", "application/json")
                .post(body)
                .build();
    }

}

