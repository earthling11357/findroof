package com.findroof.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;

import com.findroof.UsageMode;

import java.util.Map;

public class PropertyService {

    private static PropertyService instance;
    private final SharedPreferences sharedPreferences;

    public static final String SERVER_PUSH_NOTIFICATION_KEY =
            "AAAAi9ZfM5U:APA91bFxz4pxkXLLNjMbhuhlPWZicw-4zqJ9RO2Xy_52MZq66QNDstzXSB4WsLouwaknuTgXGs8tuVWnS9Zl0kVadsmzI4qaN4ctOyxc1w7qG0Xmei8lNRdZfj0Rpf4AZ3jQ2D0UEVkT";
    public static final String GETH_CLIENT_URL = "http://104.199.0.31:8545";
    public static final String LANDLORD_ADDRESS = "0xd2d03f5a8c31b3f8fc5ad8b660c7b5235b1289c0";
    public static final String LANDLORD_PASSWORD = "owner";
    public static final String TENANT_ADDRESS = "0x43c47045f09468aeddd996f133275e7ff7e9b3f7";
    public static final String TENANT_PASSWORD = "tenant";
    public static final String GOVERNMENT_ADDRESS = "0x192926b16e44742f730d660a7912432d7d4749f2";
    public static final String GOVERNMENT_PASSWORD = "government";


    private PropertyService(@NonNull SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }

    public static void init(@NonNull Context context) {
        final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        instance = new PropertyService(sharedPreferences);
    }

    public static PropertyService getInstance() {
        if (instance == null) {
            throw new IllegalStateException("PropertyService not initialized");
        }
        return instance;
    }

    public void setUsageMode(@NonNull final UsageMode usageMode) {
        setInt("usageMode", usageMode.mode());
    }

    public UsageMode getUsageMode() {
        final int usageMode = sharedPreferences.getInt("usageMode", -1);
        return UsageMode.fromMode(usageMode);
    }

    public void walletCreated(@NonNull final String walletFilename) {
        final SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("walletFilename", walletFilename);
        editor.putBoolean("walletCreated", true);
        editor.commit();
    }

    public boolean isWalletCreated() {
        return sharedPreferences.getBoolean("walletCreated", false);
    }

    public void setEjariRulesContractAddress(String address) {
        setString("ejariRulesContractAddress", address);
    }

    public String getEjariRulesContractAddress() {
        return sharedPreferences.getString("ejariRulesContractAddress", null);
    }

    public void setLandlordPushNotification(String pushNotificationId) {
        setString("landlordPushNotificationId", pushNotificationId);
    }

    public String getLandlordPushNotification() {
        return sharedPreferences.getString("landlordPushNotificationId", null);
    }

    public void setGovernmentPushNotificationId(String pushNotificationId) {
        setString("governmentPushNotificationId", pushNotificationId);
    }

    public String getGovernmentPushNotificationId() {
        return sharedPreferences.getString("governmentPushNotificationId", null);
    }

    public void setTenantPushNotificationId(String pushNotificationId) {
        setString("tenantPushNotificationId", pushNotificationId);
    }

    public String getTenantPushNotificationId() {
        return sharedPreferences.getString("tenantPushNotificationId", null);
    }

    public void setLandlordName(String name) {
        setString("landlordName", name);
    }

    public String getLandlordName() {
        return sharedPreferences.getString("landlordName", null);
    }

    public void setHouseContractAddress(String address) {
        setString("houseContractAddress", address);
    }

    public String getHouseContractAddress() {
        return sharedPreferences.getString("houseContractAddress", null);
    }

    /**
     * <strong>Use this with caution. Make sure that you have the right key names</strong>
     * @param keyValuePairs
     */
    public void setStrings(@NonNull Map<String, String> keyValuePairs) {
        final SharedPreferences.Editor editor = sharedPreferences.edit();

        for(final Map.Entry<String, String> keyValue : keyValuePairs.entrySet()) {
            editor.putString(keyValue.getKey(), keyValue.getValue());
        }

        editor.commit();
    }

    private void setString(@NonNull String key, @NonNull String value) {
        final SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    private void setBoolean(@NonNull String key, boolean value) {
        final SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    private void setInt(@NonNull String key, int value) {
        final SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(key, value);
        editor.commit();
    }

}
