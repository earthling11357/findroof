package com.findroof.util;

import android.support.annotation.NonNull;
import android.util.Log;

import com.findroof.BuildConfig;

public class Logger {

    public static void info(@NonNull final String message) {
        Log.i(getTag(), message);
    }

    /**
     * Logs message only if in Dev environment
     * @param message
     */
    public static void debug(@NonNull final String message) {
        if(BuildConfig.DEBUG) {
            Log.d(getTag(), message);
        }
    }

    public static void warn(@NonNull final String message) {
        Log.w(getTag(), message);
    }

    public static void error(@NonNull final String message) {
        Log.e(getTag(), message);
    }

    public static void error(@NonNull final String message, final Throwable throwable) {
        Log.e(getTag(), message, throwable);
    }

    /**
     * Creates the tag automatically.
     * @return The tag for the current method.
     * Sourced from https://gist.github.com/eefret/a9c7ac052854a10a8936
     */
    private static String getTag() {
        try {
            final StringBuilder sb = new StringBuilder();
            final int callerStackDepth = 4;
            final String className = Thread.currentThread().getStackTrace()[callerStackDepth].getClassName();
            sb.append(className.substring(className.lastIndexOf(".") + 1));
            sb.append("[");
            sb.append(Thread.currentThread().getStackTrace()[callerStackDepth].getMethodName());
            sb.append("] - ");
            sb.append(Thread.currentThread().getStackTrace()[callerStackDepth].getLineNumber());
            return sb.toString();
        } catch (final Exception ex) {
            Log.e("Logger", ex.getMessage());
        }
        return null;
    }

}
