package com.findroof;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.findroof.ethereum.WalletCreationService;
import com.findroof.fragment.ProgressDialogFragment;
import com.findroof.util.PropertyService;

public class CreateWalletActivity extends AppCompatActivity implements WalletCreationService.WalletCreateListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_wallet);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.create_wallet, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.createWallet:
                final TextInputEditText pinField = (TextInputEditText) findViewById(R.id.pin);
                if(TextUtils.isEmpty(pinField.getText())) {
                    ((TextInputLayout) findViewById(R.id.pinWrapper)).setError("Enter pin");
                    return true;
                }

                final ProgressDialogFragment submitProgress = new ProgressDialogFragment();
                final Bundle args = new Bundle();
                args.putString("message", "Creating wallet...");

                submitProgress.setArguments(args);
                final FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.add(submitProgress, "progressFrag")
                        .commit();

                final WalletCreationService walletCreationService = new WalletCreationService();
                walletCreationService.run(pinField.getText().toString(), getExternalFilesDir(null).getAbsolutePath(), this);

                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void walletCreated() {
        dismissProgressDialog();
        switch (PropertyService.getInstance().getUsageMode()) {
            case GOVERNMENT:
                startActivity(new Intent(this, GovernmentActivity.class));
                break;
            case TENANT:
                startActivity(new Intent(this, TenantActivity.class));
                break;
            case LANDLORD:
                startActivity(new Intent(this, LandlordActivity.class));
                break;
        }

        setResult(RESULT_OK, new Intent());
    }

    @Override
    public void walletCreationFailed() {
        dismissProgressDialog();
        Toast.makeText(this, "Unable to create wallet", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onDestroy() {
        dismissProgressDialog();

        super.onDestroy();
    }

    private void dismissProgressDialog() {
        final Fragment progressFrag = getSupportFragmentManager().findFragmentByTag("progressFrag");
        if(progressFrag != null) {
            ((ProgressDialogFragment) progressFrag).dismiss();
        }
    }
}
