package com.findroof;

import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.findroof.fragment.ProgressDialogFragment;
import com.findroof.util.Logger;
import com.findroof.util.PropertyService;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.web3j.abi.FunctionEncoder;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Function;
import org.web3j.abi.datatypes.Type;
import org.web3j.abi.datatypes.Utf8String;
import org.web3j.abi.datatypes.generated.Uint256;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.Web3jFactory;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.methods.request.Transaction;
import org.web3j.protocol.core.methods.response.EthGetTransactionCount;
import org.web3j.protocol.core.methods.response.EthGetTransactionReceipt;
import org.web3j.protocol.core.methods.response.EthSendTransaction;
import org.web3j.protocol.http.HttpService;
import org.web3j.protocol.parity.Parity;
import org.web3j.protocol.parity.ParityFactory;
import org.web3j.protocol.parity.methods.response.PersonalUnlockAccount;
import org.web3j.utils.Convert;

import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

public class GovernmentSetEjariRulesActivity extends AppCompatActivity implements OnMapReadyCallback,
        GoogleMap.OnMapClickListener {

    private GoogleMap map;
    private Marker houseMarker;
    private LatLng selectedLatLng;
    private boolean resolvingAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_government_set_ejari_rules);

        final SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_ejari_rule, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.addEjariRule:
                showProgressDialog("Adding rule...");

                final Rule newRule = new Rule(Integer.toString((int)selectedLatLng.latitude), Integer.toString((int)selectedLatLng.longitude),
                        Integer.parseInt(((TextInputEditText) findViewById(R.id.maxIncrementPercentage)).getText().toString()),
                        Integer.parseInt(((TextInputEditText) findViewById(R.id.maxRent)).getText().toString()));

                new AddEjariRule(PropertyService.getInstance().getEjariRulesContractAddress(), newRule).execute();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMapClick(LatLng latLng) {
        Logger.debug("selected latlng are: "+latLng.latitude+", "+latLng.longitude);
        resolvingAddress = true;

        final ProgressDialogFragment submitProgress = new ProgressDialogFragment();
        final Bundle args = new Bundle();
        args.putString("message", "Resolving address...");

        submitProgress.setArguments(args);
        final FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(submitProgress, "progressFrag")
                .commit();

        selectedLatLng = latLng;
        if(houseMarker != null) {
            houseMarker.remove();
        }
        houseMarker = map.addMarker(new MarkerOptions()
                .position(latLng)
                .title("House"));

        new GetAddressTask().execute(latLng);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.map = googleMap;
        googleMap.setOnMapClickListener(this);
    }

    @Override
    protected void onStop() {
        dismissProgressDialog();

        super.onStop();
    }

    private void showProgressDialog(String message) {
        final ProgressDialogFragment submitProgress = new ProgressDialogFragment();
        final Bundle args = new Bundle();
        args.putString("message", message);

        submitProgress.setArguments(args);
        final FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(submitProgress, "progressFrag")
                .commit();
    }

    private void dismissProgressDialog() {
        final Fragment progressFrag = getSupportFragmentManager().findFragmentByTag("progressFrag");
        if(progressFrag != null) {
            ((ProgressDialogFragment) progressFrag).dismiss();
        }
    }

    private class GetAddressTask extends AsyncTask<LatLng, Void, Address> {

        @Override
        protected Address doInBackground(LatLng... params) {
            try {
                final LatLng location = params[0];
                final Geocoder geocoder = new Geocoder(GovernmentSetEjariRulesActivity.this, Locale.US);
                final List<Address> addresses = geocoder.getFromLocation(
                        location.latitude,
                        location.longitude, 1);
                if(addresses != null && !addresses.isEmpty()) {
                    return addresses.get(0);
                }
            } catch (IOException exception) {
                Logger.error("Unable to get addresses", exception);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Address address) {
            String addressText = "Unable to resolve address";
            if(address != null) {
                Logger.debug(address.toString());
                addressText = "";
                for (int i = 0; i <= address.getMaxAddressLineIndex(); i++) {
                    addressText = addressText + address.getAddressLine(i);
                    if(i != address.getMaxAddressLineIndex()) {
                        addressText = addressText + "\n";
                    }
                }
            }

            ((TextView) findViewById(R.id.houseAddress)).setText(addressText);
            dismissProgressDialog();
            resolvingAddress = false;
        }
    }

    class Rule {
        String latitude;
        String longitude;
        int incrementPercentage;
        int maxRent;

        public Rule(String latitude, String longitude, int incrementPercentage, int maxRent) {
            this.latitude = latitude;
            this.longitude = longitude;
            this.incrementPercentage = incrementPercentage;
            this.maxRent = maxRent;
        }
    }

    private class AddEjariRule extends AsyncTask<Void, Void, Boolean> {

        String contractAddress;
        Rule newRule;

        AddEjariRule(String contractAddress, Rule newRule) {
            this.contractAddress = contractAddress;
            this.newRule = newRule;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                final Parity parity = ParityFactory.build(new HttpService(PropertyService.GETH_CLIENT_URL));
                final PersonalUnlockAccount personalUnlockAccount =
                        parity.personalUnlockAccount(PropertyService.GOVERNMENT_ADDRESS, PropertyService.GOVERNMENT_PASSWORD).sendAsync().get();
                if(personalUnlockAccount.accountUnlocked()) {
                    Logger.debug("account unlocked");

                    final Web3j web3j = Web3jFactory.build(new HttpService(PropertyService.GETH_CLIENT_URL));
                    final EthGetTransactionCount ethGetTransactionCount = web3j.ethGetTransactionCount(
                            PropertyService.GOVERNMENT_ADDRESS, DefaultBlockParameterName.LATEST).sendAsync().get();

                    final BigInteger nonce = ethGetTransactionCount.getTransactionCount();
                    final BigInteger gasPrice = Convert.toWei("0.02", Convert.Unit.SZABO).toBigInteger();
                    final BigInteger gasLimit = new BigInteger("4150000");
                    final BigInteger funds = new BigInteger("0");

                    final List<Type> functionParams = new ArrayList<>();
                    functionParams.add(new Utf8String(newRule.latitude));
                    functionParams.add(new Utf8String(newRule.longitude));
                    functionParams.add(new Uint256(new BigInteger(Integer.toString(newRule.incrementPercentage))));
                    functionParams.add(new Uint256(new BigInteger(Integer.toString(newRule.maxRent))));

                    final List<TypeReference<?>> functionParamTypeRefs = new ArrayList<>();
                    functionParamTypeRefs.add(TypeReference.create(Utf8String.class));
                    functionParamTypeRefs.add(TypeReference.create(Utf8String.class));
                    functionParamTypeRefs.add(TypeReference.create(Uint256.class));
                    functionParamTypeRefs.add(TypeReference.create(Uint256.class));

                    Function function = new Function("addEjariRule",
                            functionParams, functionParamTypeRefs);

                    final String encodedFunction = FunctionEncoder.encode(function);
                    Transaction transaction = Transaction.createFunctionCallTransaction(
                            PropertyService.GOVERNMENT_ADDRESS, nonce, gasPrice, gasLimit, contractAddress, funds, encodedFunction);

                    final EthSendTransaction transactionResponse =
                            web3j.ethSendTransaction(transaction).sendAsync().get();

                    final String transactionHash = transactionResponse.getTransactionHash();
                    if(transactionResponse.getError() != null) {

                        Logger.debug("tx error msg: "+transactionResponse.getError().getMessage()+", reason: "+transactionResponse.getError().getData());
                    }
                    Logger.debug("transaction hash:  " +transactionHash);

                    if(transactionHash != null) {

                        EthGetTransactionReceipt transactionReceipt;
                        do {
                            Logger.debug("getting receipt");
                            transactionReceipt = web3j.ethGetTransactionReceipt(transactionHash).sendAsync().get();//.getTransactionReceipt();
                            Thread.sleep(10000);
                        } while(transactionReceipt.getTransactionReceipt() == null);

                        if(transactionReceipt.hasError()) {
                            Logger.debug("txReceipt error msg: "+transactionReceipt.getError().getMessage()+", reason: "+transactionReceipt.getError().getData());
                        }
                        if(transactionReceipt.getTransactionReceipt() != null) {
                            Logger.debug("contract address: " + transactionReceipt.getTransactionReceipt().getContractAddress());
                        }

                        return true;
                    }

                    return false;
                } else {
                    return false;
                }
            } catch (InterruptedException exception) {
                Logger.error("Unable to set contract valid", exception);
            } catch (ExecutionException exception) {
                Logger.error("Unable to set contract valid", exception);
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            dismissProgressDialog();
            if(result) {
                Toast.makeText(GovernmentSetEjariRulesActivity.this, "Ejari Rule added to the contract", Toast.LENGTH_LONG).show();
                finish();
            } else {
                Toast.makeText(GovernmentSetEjariRulesActivity.this, "Unable to add rule to the Contract", Toast.LENGTH_LONG).show();
            }
        }
    }
}
