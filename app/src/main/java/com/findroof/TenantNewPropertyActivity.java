package com.findroof;

import android.content.Intent;
import android.nfc.NdefMessage;
import android.nfc.NfcAdapter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.findroof.fragment.ProgressDialogFragment;
import com.findroof.util.Logger;
import com.findroof.util.PropertyService;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;
import org.web3j.abi.FunctionEncoder;
import org.web3j.abi.FunctionReturnDecoder;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Function;
import org.web3j.abi.datatypes.Type;
import org.web3j.abi.datatypes.generated.Uint256;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.Web3jFactory;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.methods.request.Transaction;
import org.web3j.protocol.core.methods.response.EthCall;
import org.web3j.protocol.core.methods.response.EthGetTransactionCount;
import org.web3j.protocol.core.methods.response.EthGetTransactionReceipt;
import org.web3j.protocol.core.methods.response.EthSendTransaction;
import org.web3j.protocol.http.HttpService;
import org.web3j.protocol.parity.Parity;
import org.web3j.protocol.parity.ParityFactory;
import org.web3j.protocol.parity.methods.response.PersonalUnlockAccount;
import org.web3j.utils.Convert;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class TenantNewPropertyActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap map;
    private String houseContractAddress;
    private ContractDetails contractDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tenant_new_property);

        final SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        showProgressDialog("Getting property details...");
    }

    @Override
    public void onResume() {
        super.onResume();
        // Check to see that the Activity started due to an Android Beam
        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(getIntent().getAction())) {
            processIntent(getIntent());
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.map = googleMap;
    }

    @Override
    public void onNewIntent(Intent intent) {
        // onResume gets called after this to handle the intent
        Logger.debug("New intent recvd");
        setIntent(intent);
    }

    @Override
    protected void onStop() {
        dismissProgressDialog();

        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.pay_property, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.payProperty) {
            new PayForProperty(houseContractAddress).execute();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void populateDetails() {
        ContractDetails contractDetails = new ContractDetails("25.23363026245991", "55.367910638451576", "4.2", "4.5", new BigInteger("200"), new BigInteger("200"));
        map.addMarker(new MarkerOptions()
                .position(new LatLng(Double.parseDouble(contractDetails.latitude),
                                     Double.parseDouble(contractDetails.longitude)))
                .title("House"));

        ((TextView) findViewById(R.id.landlordRating)).setText(contractDetails.landlordAvgRating);
        ((TextView) findViewById(R.id.houseRating)).setText(contractDetails.propertyAvgRating);
        ((TextView) findViewById(R.id.rent)).setText(contractDetails.rent.toString()+" wei");
        ((TextView) findViewById(R.id.securityDeposit)).setText(contractDetails.security.toString()+" wei");
    }

    private void showProgressDialog(String message) {
        final ProgressDialogFragment submitProgress = new ProgressDialogFragment();
        final Bundle args = new Bundle();
        args.putString("message", message);

        submitProgress.setArguments(args);
        final FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(submitProgress, "progressFrag")
                .commit();
    }

    private void dismissProgressDialog() {
        final Fragment progressFrag = getSupportFragmentManager().findFragmentByTag("progressFrag");
        if(progressFrag != null) {
            ((ProgressDialogFragment) progressFrag).dismiss();
        }
    }

    /**
     * Parses the NDEF Message from the intent and prints to the TextView
     */
    void processIntent(Intent intent) {
        Parcelable[] rawMsgs = intent.getParcelableArrayExtra(
                NfcAdapter.EXTRA_NDEF_MESSAGES);
        // only one message sent during the beam
        NdefMessage msg = (NdefMessage) rawMsgs[0];
        // record 0 contains the MIME type, record 1 is the AAR, if present
        String payload = new String(msg.getRecords()[0].getPayload());
        Logger.debug("ndef payload: "+ payload);

        try {
            JSONObject jsonPayload = new JSONObject(payload);
            houseContractAddress = jsonPayload.getString("houseContractAddress");
            new GetContractDetails(houseContractAddress).execute();
        } catch (JSONException exception) {
            Logger.error("Unable to parse ndef payload");
        }
    }

    class ContractDetails {
        String latitude;
        String longitude;
        String landlordAvgRating;
        String propertyAvgRating;
        BigInteger rent;
        BigInteger security;

        public ContractDetails(String latitude, String longitude, String landlordAvgRating, String propertyAvgRating, BigInteger rent, BigInteger security) {
            this.latitude = latitude;
            this.longitude = longitude;
            this.landlordAvgRating = landlordAvgRating;
            this.propertyAvgRating = propertyAvgRating;
            this.rent = rent;
            this.security = security;
        }
    }

    private class GetContractDetails extends AsyncTask<Void, Void, ContractDetails> {

        String contractAddress;

        GetContractDetails(String contractAddress) {
            this.contractAddress = contractAddress;
        }

        @Override
        protected ContractDetails doInBackground(Void... params) {
            try {
                final Parity parity = ParityFactory.build(new HttpService(PropertyService.GETH_CLIENT_URL));
                final PersonalUnlockAccount personalUnlockAccount =
                        parity.personalUnlockAccount(PropertyService.TENANT_ADDRESS, PropertyService.TENANT_PASSWORD).sendAsync().get();
                if(personalUnlockAccount.accountUnlocked()) {
                    Logger.debug("account unlocked");

                    final Web3j web3j = Web3jFactory.build(new HttpService(PropertyService.GETH_CLIENT_URL));

                    final List<Type> functionParams = new ArrayList<>();
                    final List<TypeReference<?>> functionParamTypeRefs = new ArrayList<>();
                    Function function = new Function("getCoordinates",
                            functionParams, functionParamTypeRefs);

                    String encodedFunction = FunctionEncoder.encode(function);
                    EthCall response = web3j.ethCall(
                            Transaction.createEthCallTransaction(contractAddress, encodedFunction),
                            DefaultBlockParameterName.LATEST)
                            .sendAsync().get();

                    List<Type> returnTypes = FunctionReturnDecoder.decode(
                            response.getValue(), function.getOutputParameters());

                    for(Type returnType : returnTypes) {
                        Logger.debug(returnType.getValue() + ", " + returnType.getTypeAsString());
                    }

                    Logger.debug("getownerratings");
                    function = new Function("getOwnerRatingTuple",
                            functionParams, functionParamTypeRefs);
                    encodedFunction = FunctionEncoder.encode(function);
                    response = web3j.ethCall(
                            Transaction.createEthCallTransaction(contractAddress, encodedFunction),
                            DefaultBlockParameterName.LATEST)
                            .sendAsync().get();

                    returnTypes = FunctionReturnDecoder.decode(
                            response.getValue(), function.getOutputParameters());

                    for(Type returnType : returnTypes) {
                        Logger.debug(returnType.getValue() + ", " + returnType.getTypeAsString());
                    }

                    Logger.debug("getpropertyratings");
                    function = new Function("getPropertyRatingTuple",
                            functionParams, functionParamTypeRefs);
                    encodedFunction = FunctionEncoder.encode(function);
                    response = web3j.ethCall(
                            Transaction.createEthCallTransaction(contractAddress, encodedFunction),
                            DefaultBlockParameterName.LATEST)
                            .sendAsync().get();

                    returnTypes = FunctionReturnDecoder.decode(
                            response.getValue(), function.getOutputParameters());

                    for(Type returnType : returnTypes) {
                        Logger.debug(returnType.getValue() + ", " + returnType.getTypeAsString());
                    }


                    return null;
                } else {
                    return null;
                }
            } catch (InterruptedException exception) {
                Logger.error("Unable to set contract valid", exception);
            } catch (ExecutionException exception) {
                Logger.error("Unable to set contract valid", exception);
            }
            return null;
        }

        @Override
        protected void onPostExecute(ContractDetails contractDetails) {
            if(contractDetails != null) {
                TenantNewPropertyActivity.this.contractDetails = contractDetails;
                populateDetails();
            } else {
                TenantNewPropertyActivity.this.contractDetails =
                        new ContractDetails("25.23363026245991", "55.367910638451576", "4.2", "4.5", new BigInteger("200"), new BigInteger("200"));
            }
            dismissProgressDialog();
        }
    }

    private class PayForProperty extends AsyncTask<Void, Void, Boolean> {

        String contractAddress;

        PayForProperty(String contractAddress) {
            this.contractAddress = contractAddress;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                final Parity parity = ParityFactory.build(new HttpService(PropertyService.GETH_CLIENT_URL));
                final PersonalUnlockAccount personalUnlockAccount =
                        parity.personalUnlockAccount(PropertyService.TENANT_ADDRESS, PropertyService.TENANT_PASSWORD).sendAsync().get();
                if(personalUnlockAccount.accountUnlocked()) {
                    Logger.debug("account unlocked");

                    final Web3j web3j = Web3jFactory.build(new HttpService(PropertyService.GETH_CLIENT_URL));
                    final EthGetTransactionCount ethGetTransactionCount = web3j.ethGetTransactionCount(
                            PropertyService.TENANT_ADDRESS, DefaultBlockParameterName.LATEST).sendAsync().get();

                    final BigInteger nonce = ethGetTransactionCount.getTransactionCount();
                    final BigInteger gasPrice = Convert.toWei("0.02", Convert.Unit.SZABO).toBigInteger();
                    final BigInteger gasLimit = new BigInteger("4150000");
                    final BigInteger funds = contractDetails.rent.add(contractDetails.security);

                    final List<Type> functionParams = new ArrayList<>();
                    final Calendar now = Calendar.getInstance();

                    long nowTimeInS = now.getTimeInMillis() / 1000;

                    now.add(Calendar.MONTH, 1);
                    long toTimeInS = now.getTimeInMillis() / 1000;

                    functionParams.add(new Uint256(new BigInteger(Long.toString(nowTimeInS))));
                    functionParams.add(new Uint256(new BigInteger(Long.toString(toTimeInS))));

                    final List<TypeReference<?>> functionParamTypeRefs = new ArrayList<>();
                    functionParamTypeRefs.add(TypeReference.create(Uint256.class));
                    functionParamTypeRefs.add(TypeReference.create(Uint256.class));
                    Function function = new Function("pay",
                            functionParams, functionParamTypeRefs);

                    final String encodedFunction = FunctionEncoder.encode(function);
                    Transaction transaction = Transaction.createFunctionCallTransaction(
                            PropertyService.TENANT_ADDRESS, nonce, gasPrice, gasLimit, contractAddress, funds, encodedFunction);

                    final EthSendTransaction transactionResponse =
                            web3j.ethSendTransaction(transaction).sendAsync().get();

                    final String transactionHash = transactionResponse.getTransactionHash();
                    if(transactionResponse.getError() != null) {

                        Logger.debug("tx error msg: "+transactionResponse.getError().getMessage()+", reason: "+transactionResponse.getError().getData());
                    }
                    Logger.debug("transaction hash:  " +transactionHash);

                    if(transactionHash != null) {

                        EthGetTransactionReceipt transactionReceipt;
                        do {
                            Logger.debug("getting receipt");
                            transactionReceipt = web3j.ethGetTransactionReceipt(transactionHash).sendAsync().get();//.getTransactionReceipt();
                            Thread.sleep(10000);
                        } while(transactionReceipt.getTransactionReceipt() == null);

                        if(transactionReceipt.hasError()) {
                            Logger.debug("txReceipt error msg: "+transactionReceipt.getError().getMessage()+", reason: "+transactionReceipt.getError().getData());
                        }
                        if(transactionReceipt.getTransactionReceipt() != null) {
                            Logger.debug("contract address: " + transactionReceipt.getTransactionReceipt().getContractAddress());
                        }

                        return true;
                    }

                    return false;
                } else {
                    return false;
                }
            } catch (InterruptedException exception) {
                Logger.error("Unable to set contract valid", exception);
            } catch (ExecutionException exception) {
                Logger.error("Unable to set contract valid", exception);
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            dismissProgressDialog();
            if(result) {
                Toast.makeText(TenantNewPropertyActivity.this, "Congrats! You're now the tenant of this house", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(TenantNewPropertyActivity.this, "Unable to pay for this house", Toast.LENGTH_LONG).show();
            }
        }
    }
}
