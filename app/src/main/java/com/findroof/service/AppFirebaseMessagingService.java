package com.findroof.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import com.findroof.PropertyValidationActivity;
import com.findroof.R;
import com.findroof.util.Logger;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

public class AppFirebaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Logger.debug("from: " + remoteMessage.getFrom());

        for(Map.Entry messageEntry : remoteMessage.getData().entrySet()) {
            Logger.debug("data - key: " + messageEntry.getKey() + ", value: "+messageEntry.getValue());
        }

        final Map<String, String> data = remoteMessage.getData();
        String component = data.get("component");
        if(component.equals("property_validated")) {
            final Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            final NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getApplicationContext())
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText("Property validated")
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri);

            final NotificationManager notificationManager =
                    (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

            final Notification notification = notificationBuilder.build();
            notification.defaults |= Notification.DEFAULT_ALL; //default vibrate, sound and lights
            notificationManager.notify(1, notification);
        } else if(component.equals("new_property")) {
            String latitude = data.get("latitude");
            String longitude = data.get("longitude");
            String landlordName = data.get("landlordName");
            String propertyType = data.get("propertyType");
            String bedroomType = data.get("bedroomType");
            String contractAddress = data.get("contractAddress");

            final Intent propertyValidationActivityIntent = new Intent(getApplicationContext(), PropertyValidationActivity.class);
            propertyValidationActivityIntent.putExtra("latitude", latitude);
            propertyValidationActivityIntent.putExtra("longitude", longitude);
            propertyValidationActivityIntent.putExtra("landlordName", landlordName);
            propertyValidationActivityIntent.putExtra("propertyType", propertyType);
            propertyValidationActivityIntent.putExtra("bedroomType", bedroomType);
            propertyValidationActivityIntent.putExtra("contractAddress", contractAddress);

            propertyValidationActivityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getApplicationContext().startActivity(propertyValidationActivityIntent);
        }

    }
}
