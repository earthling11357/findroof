package com.findroof.service;

import com.findroof.UsageMode;
import com.findroof.util.Logger;
import com.findroof.util.PropertyService;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;


public class AppFirebaseInstanceIdService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        final String token = FirebaseInstanceId.getInstance().getToken();
        if(token != null) {
            UsageMode usageMode = PropertyService.getInstance().getUsageMode();
            final FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
            switch(usageMode) {
                case GOVERNMENT:{
                    final DatabaseReference governmentPNID = firebaseDatabase.getReference("governmentPNID");
                    governmentPNID.setValue(token);
                    break;
                }
                case TENANT:{
                    final DatabaseReference tenantPNID = firebaseDatabase.getReference("tenantPNID");
                    tenantPNID.setValue(token);
                    break;
                }
                case LANDLORD:{
                    final DatabaseReference landlordPNID = firebaseDatabase.getReference("landlordPNID");
                    landlordPNID.setValue(token);
                    break;
                }
            }
            Logger.debug("New token received");

        }
    }
}
