package com.findroof.fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatDialogFragment;
import android.text.TextUtils;

public class ProgressDialogFragment extends AppCompatDialogFragment {

    private String mTitle;
    private String mMessage;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCancelable(false);
    }

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Bundle args;
        if(savedInstanceState != null) {
            args = savedInstanceState;
        } else {
            args = getArguments();
        }


        final ProgressDialog progressDialog = new ProgressDialog(getActivity(), getTheme());
        mTitle = args.getString("title");
        if(!TextUtils.isEmpty(mTitle)) {
            progressDialog.setTitle(mTitle);
        }

        mMessage = args.getString("message");
        if(TextUtils.isEmpty(mMessage)) {
            throw new IllegalArgumentException();
        }

        progressDialog.setMessage(mMessage);
        progressDialog.setIndeterminate(true);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        return progressDialog;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString("title", mTitle);
        outState.putString("message", mMessage);
    }
}
