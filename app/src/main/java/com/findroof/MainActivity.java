package com.findroof;

import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.NfcEvent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.findroof.util.Logger;

import static android.nfc.NdefRecord.createMime;

public class MainActivity extends AppCompatActivity implements NfcAdapter.CreateNdefMessageCallback {

    private NfcAdapter mNfcAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //new Web3JRequest().execute();

        findViewById(R.id.sendData).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Check for available NFC Adapter
                mNfcAdapter = NfcAdapter.getDefaultAdapter(MainActivity.this);
                if (mNfcAdapter == null) {
                    Toast.makeText(MainActivity.this, "NFC is not available", Toast.LENGTH_LONG).show();
                    finish();
                    return;
                }
                // Register callback
                mNfcAdapter.setNdefPushMessageCallback(MainActivity.this, MainActivity.this);
            }
        });
    }

    @Override
    public void onDestroy() {


        super.onDestroy();
    }

    @Override
    public NdefMessage createNdefMessage(NfcEvent event) {
        Logger.debug("Creating ndef msg");
        final String text = ("Beam me up, Android!\n\n" +
                "Beam Time: " + System.currentTimeMillis());
        final NdefMessage msg = new NdefMessage(
                    new NdefRecord[] {
                            createMime("application/vnd.com.findroof.android.beam",
                                        text.getBytes()
                            ),
                            NdefRecord.createApplicationRecord("com.findroof")
                    });

        return msg;
    }

}
